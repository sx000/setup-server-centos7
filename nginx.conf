#https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-centos-7
user  www-data;
worker_processes        auto;


error_log  logs/error.log;
error_log  logs/error.log  notice;
error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    
    pagespeed FileCachePath /var/ngx_pagespeed_cache;
    pagespeed GlobalStatisticsPath /ngx_pagespeed_global_statistics;
    pagespeed FileCacheSizeKb            50000000;
    pagespeed FileCacheCleanIntervalMs   -1; #3600000 = 1 chas
    pagespeed FileCacheInodeLimit        50000000; #500000 = 50MB
    pagespeed MessageBufferSize 200000;

    pagespeed GlobalAdminPath /pagespeed_global_admin;
    
    
    pagespeed StatisticsLoggingIntervalMs 60000;
    pagespeed StatisticsLoggingMaxFileSizeKb 1024;
    #pagespeed CriticalImagesBeaconEnabled true;
    
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    gzip  on;

    server {
        ## Pagespeed Settings
        pagespeed on;
        pagespeed EnableFilters resize_rendered_image_dimensions;
        pagespeed EnableFilters resize_images;
        pagespeed JpegRecompressionQualityForSmallScreens 45;
        pagespeed DisableFilters convert_jpeg_to_webp,convert_to_webp_lossless;
        #
        # Ensure requests for pagespeed optimized resources go to the pagespeed handler and no extraneous headers get set.
        location ~ "\.pagespeed\.([a-z]\.)?[a-z]{2}\.[^.]{10}\.[^.]+" {
          add_header "" "";
        }

        location /ngx_pagespeed_statistics { allow 51.255.36.125; allow 188.42.231.80; deny all; }
        location /ngx_pagespeed_message { allow 51.255.36.125; allow 188.42.231.80; deny all; }
        location /pagespeed_console { allow 51.255.36.125; allow 188.42.231.80; deny all; }
        location ~ ^/pagespeed_admin { allow 51.255.36.125; allow 188.42.231.80; deny all; }
        location /ngx_pagespeed_global_statistics { allow 51.255.36.125; allow 188.42.231.80; deny all; }
        location ~ ^/pagespeed_global_admin { allow 51.255.36.125; allow 188.42.231.80; deny all; }
        
        pagespeed Statistics on;
        pagespeed StatisticsLogging on;
        pagespeed LogDir /var/log/pagespeed;
        pagespeed EnableFilters insert_ga;
        pagespeed AnalyticsID UA-88108131-17;
        pagespeed EnableFilters remove_comments;
        pagespeed EnableFilters collapse_whitespace;


        pagespeed StatisticsPath /ngx_pagespeed_statistics;
        pagespeed MessagesPath /ngx_pagespeed_message;
        pagespeed ConsolePath /pagespeed_console;
        pagespeed AdminPath /pagespeed_admin;
        pagespeed EnableCachePurge on;
        
        listen    80;
        server_name www.weliveluxury.com;

        # note that these lines are originally from the "location /" block
        #root   /usr/share/nginx/html;

        root   /home/pavel/www.weliveluxury.com/www;
        
        index index.php index.html index.htm;

        location / {
            try_files $uri $uri/ =404;
        }
        error_page 404 /404.html;
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
            root /usr/share/nginx/html;
        }

        location ~ \.php$ {
            try_files $uri =404;
            fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
    }


    # another virtual host using mix of IP-, name-, and port-based configuration
    #
    #server {
    #    listen       8000;
    #    listen       somename:8080;
    #    server_name  somename  alias  another.alias;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}


    # HTTPS server
    #
    #server {
    #    listen       443 ssl;
    #    server_name  localhost;

    #    ssl_certificate      cert.pem;
    #    ssl_certificate_key  cert.key;

    #    ssl_session_cache    shared:SSL:1m;
    #    ssl_session_timeout  5m;

    #    ssl_ciphers  HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}

}
