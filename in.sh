#used https://www.linuxtweaks.in/install-nginx-with-pagespeed-in-centos-redhat/
#setup LEMP: https://www.8host.com/blog/ustanovka-lemp-stack-na-centos-7/

yum update -y
yum upgrade -y
yum install gcc-c++ pcre-dev pcre-devel zlib-devel make wget unzip openssl openssl-devel uuid-devel libuuid-devel -y
cd ~

#chesk new version this https://www.modpagespeed.com/doc/release_notes
NPS_VERSION=1.13.35.2
wget https://github.com/pagespeed/ngx_pagespeed/archive/v${NPS_VERSION}-stable.zip
unzip v${NPS_VERSION}-stable.zip
cd $HOME/incubator-pagespeed-ngx-${NPS_VERSION}-stable/

wget https://dl.google.com/dl/page-speed/psol/${NPS_VERSION}-x64.tar.gz
tar -xzvf ${NPS_VERSION}-x64.tar.gz
cd ~

#chesk new version this https://github.com/openresty/headers-more-nginx-module#version
HM_VERSION=0.33
wget https://github.com/agentzh/headers-more-nginx-module/archive/v${HM_VERSION}.tar.gz
tar -xvzf v${HM_VERSION}.tar.gz

#chesk new version this http://nginx.org/en/download.html
NGINX_VERSION=1.13.12
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
tar -xvzf nginx-${NGINX_VERSION}.tar.gz
cd nginx-${NGINX_VERSION}/

mkdir /var/log/access/

./configure \
        --prefix=/usr/share/nginx \
        --sbin-path=/usr/sbin/nginx \
        --conf-path=/etc/nginx/nginx.conf \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/lock/nginx.lock \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/access/access.log \
        --user=www-data \
        --group=www-data \
        --with-http_ssl_module \
        --with-http_gzip_static_module \
        --add-module=$HOME/incubator-pagespeed-ngx-${NPS_VERSION}-stable \
        --add-module=$HOME/headers-more-nginx-module-${HM_VERSION}\

make
make install

mv ~/setup-server-centos7/nginx /etc/init.d/
chmod 755 /etc/init.d/nginx

mkdir /usr/share/nginx/logs/
mkdir /var/ngx_pagespeed_cache
chmod 755 /var/ngx_pagespeed_cache

chkconfig --add nginx
chkconfig nginx on

firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

#service nginx start

#firewall-cmd --zone=public --add-port=80/tcp --permanent
#firewall-cmd --zone=public --add-port=443/tcp --permanent
#firewall-cmd --reload

echo -e "[mariadb]\nname = MariaDB\nbaseurl = http://yum.mariadb.org/10.1/centos7-amd64\ngpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB\ngpgcheck=1" >> /etc/yum.repos.d/MariaDB.repo

yum install mariadb-server mariadb -y
systemctl start mariadb.service
systemctl enable mariadb.service

rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

yum install php70w git-core php70w-gd php70w-curl php70w-mbstring php70w-xml php70w-json php70w-mcrypt php70w-mysql php70w-fpm php70w-cli -y

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

#mysql_secure_installation

mv ~/setup-server-centos7/nginx.conf /etc/nginx/
mv ~/setup-server-centos7/php.ini /etc/
mv ~/setup-server-centos7/www.conf /etc/php-fpm.d/
mv ~/setup-server-centos7/access /etc/logrotate.d/
mv ~/setup-server-centos7/nginxlog /etc/logrotate.d/

service nginx start
systemctl enable php-fpm
systemctl start php-fpm

mkdir -p /home/pavel/www.weliveluxury.com/{www,log}
chown -R pavel: /home/pavel/www.weliveluxury.com/www
chmod -R 755 /home/pavel
usermod -a -G pavel www-data
echo '<?php phpinfo(); ?>' > /home/pavel/www.weliveluxury.com/www/index.php

#test Nginx
nginx -V
php -v

#/etc/selinux/config SELINUX=disabled изменить
#https://www.shellhacks.com/ru/disable-selinux/
#http://qaru.site/questions/50223/file-permissions-for-laravel-5-and-others - права для Laravel